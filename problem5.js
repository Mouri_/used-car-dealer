let data = require('./data.js');
let getYears = require('./problem4.js');

let years = getYears(data);
let result = [];
function carsBefore(year){
    for(let i=0; i<years.length; i++){
        if(years[i]==year){
            break;
        }
        result.push(years[i]);
    }
    return result;
}
module.exports = carsBefore;
