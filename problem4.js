function getYears(inventory){
    let result = [];
    for(let i=0; i<inventory.length; i++){
        result.push(inventory[i].car_year);
    }
    result.sort();
    return result;
}
module.exports = getYears;